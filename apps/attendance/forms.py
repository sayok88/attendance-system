from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.forms import ModelForm
from .models import *
class LoginForm(AuthenticationForm):
    """Login form."""

    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(
            attrs={'class': 'mdl-textfield__input', 'name': 'username'}))
    password = forms.CharField(
        label="Password",
        max_length=30,
        widget=forms.PasswordInput(
            attrs={'class': 'mdl-textfield__input', 'name': 'password'}))


class BasketForm(forms.Form):
    rideschoices = forms.CharField(
        widget=forms.HiddenInput(
            attrs={
                'class': 'form-control',
                'name': 'rideschoices', 'value': '[]'}))
