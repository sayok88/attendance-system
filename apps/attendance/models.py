from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class user_attendance(models.Model):
    user = models.ForeignKey(User)
    record_date = models.DateField()
    in_time = models.TimeField()
    out_time = models.TimeField(null=True)
    ip=models.CharField(max_length=16)