from django.shortcuts import render
from .forms import *
from django.contrib.auth.decorators import login_required
from django.contrib import auth

from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponseRedirect, HttpResponse
from datetime import datetime, timedelta
from .models import *
import json,math
# Create your views here.
def employeedetails(request):
    usrs=User.objects.all()
    form1=BasketForm()
    atts=""
    basketitems=""
    selusr=""
    avghrs = 0.0
    if request.method == 'POST':
        basketitems = json.loads(request.POST["rideschoices"])
        uid=basketitems["user"]
        month=basketitems["month"]
        year=basketitems["year"]
        atts2=user_attendance.objects.filter(user__id=uid,record_date__year=year,record_date__month=month)
        selusr=User.objects.get(id=uid)
        atts=[]
        for a in atts2:
            td=  datetime.combine(a.record_date,a.out_time) -datetime.combine(a.record_date,a.in_time)
            td1=format(td.seconds/3600.0, '.2f')
            avghrs=avghrs+float(td1)
            atts.append({'date':a.record_date,'in':a.in_time,'out':a.out_time,'hours':td1})
        avghrs=avghrs/atts2.count()
    return render(request, "apps/attendance/employeedetails.html", {'usrs': usrs,'form1':form1,"atts":atts,"avgh":avghrs,'curusr':selusr})


def home(request):
    """Home page."""
    print(request.user)

    if not request.user.is_authenticated():
        return HttpResponseRedirect("/elogin")
    ip=""
    errmsg=""
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        print(ip)
    except:
        print("err")
    current_user = auth.get_user(request)

    atts=user_attendance.objects.filter(user=request.user,record_date = datetime.now().date()).first()
    print(atts)
    if not atts:
        catts=user_attendance.objects.filter(record_date = datetime.now().date())
        flag=0
        for a in catts:
            if a.ip==ip:
                flag=1
        if flag==1:
            errmsg="This ip has already been used by other user today"
        else:
            v=user_attendance.objects.create(user=request.user,record_date =datetime.now().date(),in_time=datetime.now().time(),out_time=datetime.now().time(),ip=ip)
            v.save()
    else:
        if atts.ip==ip:
            atts.out_time=datetime.now().time()
            atts.save()
        else:
            errmsg="Your in time ip and out ip doesnt match, try from original pc or contact sayok"
    atts = user_attendance.objects.filter(user=request.user, record_date=datetime.now().date()).first()
    allatt=user_attendance.objects.filter(user=request.user).exclude(record_date=datetime.now().date())
    return render(request, "apps/attendance/home.html", {'todaysattendance':atts,'errmsg':errmsg,'allatt':allatt})


def elogin(request):
    errmsg = ""
    next = "/"
    if request.GET:
        next = request.GET['next']
        print(next)
        request.session['next'] = next
    if request.method == 'POST':
        form2 = LoginForm(request.POST)
        print(request.POST)
        print(form2)
        next = request.session.get('next', "")
        username = request.POST['username']
        password = request.POST['password']
        print(username)
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request,user)
            print("here")
            return HttpResponseRedirect("/")
        else:
            errmsg = "Error in username and password combination"
    form1 = LoginForm()
    return render(request, "apps/attendance/login.html", {'form': form1, 'errmsg': errmsg})

