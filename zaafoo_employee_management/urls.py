from django.conf.urls import *
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'zaafoo_employee_management.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.attendance.urls')),
]
